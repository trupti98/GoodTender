*How to run a Project
1. Download and extract zip file.
2. Create project into your system with name GoodTender.
3.Replace your project src folder with the downloaded one.
4.Run the project with command ng serve --open.

*Software Requirements
1) NodeJS
2) Angular CLI
3) Visual Studio Code.

NOTE: Refer angular.io website for installation and configuration.